#[macro_use]
extern crate gfx;

extern crate gfx_window_glutin;
extern crate glutin;

use gfx::traits::FactoryExt;
use gfx::Device;
use gfx_window_glutin as gfx_glutin;
use glutin::{GlContext, GlRequest};
use glutin::Api::OpenGlEs;


extern crate specs;
#[macro_use]
extern crate specs_derive;
use specs::Component;
use specs_derive::*;

mod components {
	#[derive(Component)]
	struct Player;
	
	#[derive(Component)]
	struct Transform;
	
	#[derive(Component)]
	struct Mesh;
}

use std::fs::File;
use std::io::Read;
use std::path::Path;

pub type ColorFormat = gfx::format::Srgba8;
pub type DepthFormat = gfx::format::DepthStencil;

const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];

gfx_defines!{
	vertex Vertex {
		pos: [f32; 4] = "a_Pos",
		color: [f32; 3] = "a_Color",
	}

	constant Transform {
		transform: [[f32; 4];4] = "u_Transform",
	}

	pipeline pipe {
		vbuf: gfx::VertexBuffer<Vertex> = (),
		transform: gfx::ConstantBuffer<Transform> = "Transform",
		out: gfx::RenderTarget<ColorFormat> = "Target0",
	}
}

pub fn main() {
	let cat : fn(&str)->Result<Vec<u8>,std::io::Error> = |path|{
		let mut buf : Vec<u8> = Vec::new();
		File::open(path)?.read_to_end(&mut buf)?;
		Ok(buf)
	};
	
	let mut events_loop = glutin::EventsLoop::new();
	let windowbuilder = glutin::WindowBuilder::new()
		.with_title("Triangle Example".to_string())
		.with_dimensions(512, 512);
	let contextbuilder = glutin::ContextBuilder::new()
		.with_gl(GlRequest::Specific(OpenGlEs, (2, 0)))
		.with_vsync(true);
	let (window, mut device, mut factory, color_view, mut depth_view) =
		gfx_glutin::init::<ColorFormat, DepthFormat>(windowbuilder, contextbuilder, &events_loop);

	let pso = factory.create_pipeline_simple(
		&cat("shaders/shader.glslv").unwrap()[..],
		&cat("shaders/shader.glslf").unwrap()[..],
		pipe::new()
	).unwrap();

	let mut running = true;
	while running {
		events_loop.poll_events(|event| {
			if let glutin::Event::WindowEvent { event, .. } = event {
				match event {
					glutin::WindowEvent::Closed
					| glutin::WindowEvent::KeyboardInput {
						input:
							glutin::KeyboardInput {
								virtual_keycode: Some(glutin::VirtualKeyCode::Escape),
								..
							},
						..
					} => running = false,
					_ => {}
				}
			}
		});

		window.swap_buffers().unwrap();
		device.cleanup();
	}
}
